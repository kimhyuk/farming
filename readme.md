## what is farming ??

이더리움 web3js를 통해서 이더리움 네트워크로 네이버페이,카카오페이 등과 같은
오프라인 결제를 이더리움을 통해서 결제하는 프로젝트이다.

### 사용한 개발 스택

#### 웹서버 스택

- docker-compose
- docker
- strapi framework
- rest
- mongodb

#### android(구매자 앱) 스택

- vue
- nativescript-vue
- vuex
- vue-router

### windows(판매자 어플리케이션) 스택

- vue
- electron-vue
- web3js
- vuex
- vue-router

## 프로젝트 화면

### android

#### 로그인 화면

![Alt text](/images/buyer_login.png "로그인 화면")

#### signup

![Alt text](/images/buyer_signup.png "signup")

#### 잔고 화면

![Alt text](/images/buyer_balance.png "잔고 화면")

#### 충전완료 화면

![Alt text](/images/buyer_pyament_success.jpg "마이페이지 화면")

#### qrcode 결제화면

![Alt text](/images/buyer_payment_success.jpg "qrcode 결제 화면")

#### sidebar

![Alt text](/images/buyer_sidebar.png "sidebar")

### windows app(판매자)

#### 로그인 화면

![Alt text](/images/seller_login.png "로그인 화면")

#### 회원가입 화면

![Alt text](/images/seller_signup.png "회원가입 화면")

#### 판매자 결제 화면

![Alt text](/images/seller_pay.png "판매자 결제 화면")

#### 판매목록 화면

![Alt text](/images/seller_items.png "판매자 판매목록 화면")

#### qrcode 화면

![Alt text](/images/seller_qrcode.png "판매자 qrcode")

#### 결제완료 목록 화면

![Alt text](/images/seller_payment_list.png "판매자 결제완료목록 화면")
